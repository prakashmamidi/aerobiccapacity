//
//  ViewController.swift
//  Aerobic Capacity
//
//  Created by Mamidi,Sai Prakash Reddy on 4/16/19.
//  Copyright © 2019 Mamidi,Sai Prakash Reddy. All rights reserved.
//

import UIKit

class V02MaxViewController: UIViewController {

    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var sexTF: UITextField!
    @IBOutlet weak var timeTF: UITextField!
    @IBOutlet weak var heartRateTF: UITextField!
    @IBOutlet weak var V02MaxLBL: UILabel!
    @IBOutlet weak var classificationLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func calculateBTN(_ sender: Any) {

        if let weight = Double(weightTF.text!),let age = Double(ageTF.text!),let sex = Double(sexTF.text!), let time = Double(timeTF.text!),let heartRate = Double(heartRateTF.text!){
            if weight>=0 && age>=0 && sex>=0 && time>=0 && heartRate>=0{
            let value1 = SportsGuru.shared.v02Max(weight: weight, age: age, sex: sex, time: time, heartRate: heartRate)
            V02MaxLBL.text! = String(format: "%.1f", value1)
            classificationLBL.text! = String(SportsGuru.shared.vo2MaxClassification(v02Max: value1))
                classificationLBL.textColor = SportsGuru.shared.colorToDisplay(v02Max: value1)
            }else{
                displayMessage2()
            }
        }else{
            displayMessage1()
        }
    }
    func displayMessage1(){
        let alert = UIAlertController(title: "Warning",
                                      message: "Enter a numeric value",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    func displayMessage2(){
        let alert = UIAlertController(title: "Warning",
                                      message: "The values cannot be negative",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

