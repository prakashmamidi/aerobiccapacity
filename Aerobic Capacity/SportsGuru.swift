//
//  SportsGuru.swift
//  Aerobic Capacity
//
//  Created by Mamidi,Sai Prakash Reddy on 4/16/19.
//  Copyright © 2019 Mamidi,Sai Prakash Reddy. All rights reserved.
//

import Foundation
import UIKit
struct SportsGuru {
    private var weight:Double
    private var age: Double
    private var sex: Double
    private var time: Double
    private var heartRate: Double
    
    private init(weight:Double,age:Double,sex:Double,time:Double,heartRate:Double){
        self.weight = weight
        self.age = age
        self.sex = sex
        self.time = time
        self.heartRate = heartRate
    }
    private init(){
          self.init(weight: 0.0, age: 0, sex: 0, time: 0, heartRate: 0)
    }
    
    static let shared = SportsGuru()
    
    func v02Max(weight:Double,age:Double,sex:Double,time:Double,heartRate:Double) -> Double {
        return 132.853 - (0.0769*weight)-(0.3877*age)+(6.3150*sex)-(3.2649*time)-(0.1565*heartRate)
    }
    func vo2MaxClassification(v02Max:Double) -> String {
        var result:String = ""
        if v02Max>=60.0 {
            result = "Excellent"
        }else if v02Max<60 && v02Max >= 52.0{
            result = "Good"
        }else if v02Max<52.0 && v02Max >= 47.0{
            result = "Above Average"
        }else if v02Max<47.0 && v02Max >= 0{
            result = "Average"
        }
        return result
    }
    
    func colorToDisplay(v02Max:Double) -> UIColor{
        var colorResult:UIColor = UIColor.black
        if v02Max >= 60.0{
            colorResult = UIColor.yellow
        }else if v02Max<60 && v02Max >= 52.0{
            colorResult = UIColor.blue
        }else if v02Max<52.0 && v02Max >= 47.0{
            colorResult = UIColor.green
        }else if v02Max<47.0 && v02Max >= 0{
            colorResult = UIColor.black
        }        
        return colorResult
        
    }
    
}
